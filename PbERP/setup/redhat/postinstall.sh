#!/bin/sh

set -e

primebizness_CONFIGURATION_DIR=/etc/primebizness
primebizness_CONFIGURATION_FILE=$primebizness_CONFIGURATION_DIR/primebizness.conf
primebizness_DATA_DIR=/var/lib/primebizness
primebizness_GROUP="primebizness"
primebizness_LOG_DIR=/var/log/primebizness
primebizness_LOG_FILE=$primebizness_LOG_DIR/primebizness-server.log
primebizness_USER="primebizness"

if ! getent passwd | grep -q "^primebizness:"; then
    groupadd $primebizness_GROUP
    adduser --system --no-create-home $primebizness_USER -g $primebizness_GROUP
fi
# Register "$primebizness_USER" as a postgres user with "Create DB" role attribute
su - postgres -c "createuser -d -R -S $primebizness_USER" 2> /dev/null || true
# Configuration file
mkdir -p $primebizness_CONFIGURATION_DIR
# can't copy debian config-file as addons_path is not the same
if [ ! -f $primebizness_CONFIGURATION_FILE ]
then
    echo "[options]
; This is the password that allows database operations:
; admin_passwd = admin
db_host = False
db_port = False
db_user = $primebizness_USER
db_password = False
addons_path = /usr/lib/python2.7/site-packages/primebizness/addons
" > $primebizness_CONFIGURATION_FILE
    chown $primebizness_USER:$primebizness_GROUP $primebizness_CONFIGURATION_FILE
    chmod 0640 $primebizness_CONFIGURATION_FILE
fi
# Log
mkdir -p $primebizness_LOG_DIR
chown $primebizness_USER:$primebizness_GROUP $primebizness_LOG_DIR
chmod 0750 $primebizness_LOG_DIR
# Data dir
mkdir -p $primebizness_DATA_DIR
chown $primebizness_USER:$primebizness_GROUP $primebizness_DATA_DIR

INIT_FILE=/lib/systemd/system/primebizness.service
touch $INIT_FILE
chmod 0700 $INIT_FILE
cat << EOF > $INIT_FILE
[Unit]
Description=PrimeBizness Open Source ERP and CRM
After=network.target

[Service]
Type=simple
User=primebizness
Group=primebizness
ExecStart=/usr/bin/primebizness --config $primebizness_CONFIGURATION_FILE --logfile $primebizness_LOG_FILE

[Install]
WantedBy=multi-user.target
EOF
easy_install pyPdf vatnumber pydot psycogreen suds ofxparse XlsxWriter
