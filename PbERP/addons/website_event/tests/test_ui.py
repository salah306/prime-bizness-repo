import primebizness.tests


@primebizness.tests.common.at_install(False)
@primebizness.tests.common.post_install(True)
class TestUi(primebizness.tests.HttpCase):
    def test_admin(self):
        self.phantom_js("/", "primebizness.__DEBUG__.services['web_tour.tour'].run('event')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.event.ready", login='admin')
