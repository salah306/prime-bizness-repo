# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

import primebizness.tests


@primebizness.tests.common.at_install(False)
@primebizness.tests.common.post_install(True)
class TestUi(primebizness.tests.HttpCase):
    def test_admin(self):
        self.phantom_js("/", "primebizness.__DEBUG__.services['web_tour.tour'].run('blog')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.blog.ready", login='admin')
