# Translation of PrimeBizness Server.
# This file contains the translation of the following modules:
# * contacts
# 
# Translators:
# Raul Tlaxcala Trujillo <rtlaxcala@gmail.com>, 2016
# David de León <leceda09@gmail.com>, 2016
# Martin Trigaux <mat@primebizness.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: PrimeBizness Server 10.0c\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 08:56+0000\n"
"PO-Revision-Date: 2016-09-07 08:56+0000\n"
"Last-Translator: Martin Trigaux <mat@primebizness.com>, 2016\n"
"Language-Team: Spanish (https://www.transifex.com/primebizness/teams/41243/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: contacts
#: model:ir.actions.act_window,help:contacts.action_contacts
msgid "Click to add a contact in your contacts directory."
msgstr "Haga clic para agregar un contacto en su directorio de contactos."

#. module: contacts
#: model:ir.actions.act_window,name:contacts.action_contacts
#: model:ir.ui.menu,name:contacts.menu_contacts
msgid "Contacts"
msgstr "Contactos"

#. module: contacts
#: model:ir.actions.act_window,help:contacts.action_contacts
msgid ""
"PrimeBizness helps you easily track all activities related to\n"
"            a customer: discussions, history of business opportunities,\n"
"            documents, etc."
msgstr ""
"PrimeBizness le ayuda a rastrear fácilmente toda la actividad relacionada a\n"
"un cliente: discusiones, historia de las oportunidades de negocio,\n"
"documentos, etc."
