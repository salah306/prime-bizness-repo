# -*- coding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

import primebizness
import primebizness.tests


class TestUiTranslate(primebizness.tests.HttpCase):
    def test_admin_tour_rte_translator(self):
        self.phantom_js("/", "primebizness.__DEBUG__.services['web_tour.tour'].run('rte_translator')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.rte_translator.ready", login='admin', timeout=120)


class TestUi(primebizness.tests.HttpCase):

    post_install = True
    at_install = False

    def test_01_public_homepage(self):
        self.phantom_js("/", "console.log('ok')", "'website.snippets.animation' in primebizness.__DEBUG__.services")

    def test_02_admin_homepage(self):
        self.phantom_js("/", "console.log('ok')", "'website.snippets.editor' in primebizness.__DEBUG__.services", login='admin')

    def test_03_admin_tour_banner(self):
        self.phantom_js("/", "primebizness.__DEBUG__.services['web_tour.tour'].run('banner')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.banner.ready", login='admin')
