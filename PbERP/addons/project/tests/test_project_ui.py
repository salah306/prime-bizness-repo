# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

import primebizness.tests


@primebizness.tests.common.at_install(False)
@primebizness.tests.common.post_install(True)
class TestUi(primebizness.tests.HttpCase):

    def test_01_project_tour(self):
        self.phantom_js("/web", "primebizness.__DEBUG__.services['web_tour.tour'].run('project_tour')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.project_tour.ready", login="admin")
