# -*- coding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

from primebizness import fields, models


class BaseConfigSettings(models.TransientModel):
    _inherit = 'base.config.settings'

    ldaps = fields.One2many(related='company_id.ldaps', string="LDAP Parameters *")
