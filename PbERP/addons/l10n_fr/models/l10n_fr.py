# -*- coding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

from primebizness import fields, models


class ResCompany(models.Model):
    _inherit = 'res.company'

    siret = fields.Char(string='SIRET', size=14)
    ape = fields.Char(string='APE')
