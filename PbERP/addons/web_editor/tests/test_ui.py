# -*- coding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

import primebizness.tests

@primebizness.tests.common.at_install(False)
@primebizness.tests.common.post_install(True)
class TestUi(primebizness.tests.HttpCase):

    post_install = True
    at_install = False

    def test_01_admin_rte(self):
        self.phantom_js("/web", "primebizness.__DEBUG__.services['web_tour.tour'].run('rte')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.rte.ready", login='admin')

    def test_02_admin_rte_inline(self):
        self.phantom_js("/web", "primebizness.__DEBUG__.services['web_tour.tour'].run('rte_inline')", "primebizness.__DEBUG__.services['web_tour.tour'].tours.rte_inline.ready", login='admin')
