# -*- coding: utf-8 -*-

import primebizness

def migrate(cr, version):
    registry = primebizness.registry(cr.dbname)
    from primebizness.addons.account.models.chart_template import migrate_tags_on_taxes
    migrate_tags_on_taxes(cr, registry)
