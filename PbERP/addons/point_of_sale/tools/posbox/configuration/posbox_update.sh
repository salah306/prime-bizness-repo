#!/usr/bin/env bash

sudo mount -o remount,rw /
sudo git --work-tree=/home/pi/primebizness/ --git-dir=/home/pi/primebizness/.git pull
sudo mount -o remount,ro /
(sleep 5 && sudo reboot) &
