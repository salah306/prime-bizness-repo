[![Build Status](http://runbot.primebizness.com/runbot/badge/flat/1/10.0.svg)](http://runbot.primebizness.com/runbot)
[![Tech Doc](http://img.shields.io/badge/10.0-docs-875A7B.svg?style=flat)](http://www.primebizness.com/documentation/10.0)
[![Help](http://img.shields.io/badge/10.0-help-875A7B.svg?style=flat)](https://www.primebizness.com/forum/help-1)
[![Nightly Builds](http://img.shields.io/badge/10.0-nightly-875A7B.svg?style=flat)](http://nightly.primebizness.com/)

PrimeBizness
----

PrimeBizness is a suite of web based open source business apps.

The main PrimeBizness Apps include an <a href="https://www.primebizness.com/page/crm">Open Source CRM</a>,
<a href="https://www.primebizness.com/page/website-builder">Website Builder</a>,
<a href="https://www.primebizness.com/page/e-commerce">eCommerce</a>,
<a href="https://www.primebizness.com/page/warehouse">Warehouse Management</a>,
<a href="https://www.primebizness.com/page/project-management">Project Management</a>,
<a href="https://www.primebizness.com/page/accounting">Billing &amp; Accounting</a>,
<a href="https://www.primebizness.com/page/point-of-sale">Point of Sale</a>,
<a href="https://www.primebizness.com/page/employees">Human Resources</a>,
<a href="https://www.primebizness.com/page/lead-automation">Marketing</a>,
<a href="https://www.primebizness.com/page/manufacturing">Manufacturing</a>,
<a href="https://www.primebizness.com/page/purchase">Purchase Management</a>,
<a href="https://www.primebizness.com/#apps">...</a>

PrimeBizness Apps can be used as stand-alone applications, but they also integrate seamlessly so you get
a full-featured <a href="https://www.primebizness.com">Open Source ERP</a> when you install several Apps.


Getting started with PrimeBizness
-------------------------
For a standard installation please follow the <a href="https://www.primebizness.com/documentation/10.0/setup/install.html">Setup instructions</a>
from the documentation.

If you are a developer you may type the following command at your terminal:

    wget -O- https://raw.githubusercontent.com/primebizness/primebizness/10.0/setup/setup_dev.py | python

Then follow <a href="https://www.primebizness.com/documentation/10.0/tutorials.html">the developer tutorials</a>


For PrimeBizness employees
------------------

To add the primebizness-dev remote use this command:

    $ ./setup/setup_dev.py setup_git_dev

To fetch primebizness merge pull requests refs use this command:

    $ ./setup/setup_dev.py setup_git_review

