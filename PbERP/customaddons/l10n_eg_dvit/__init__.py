# -*- encoding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

from primebizness import api, SUPERUSER_ID

def load_translations(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    env.ref('l10n_eg_dvit.account_egypt_coa_general').process_coa_translations()
