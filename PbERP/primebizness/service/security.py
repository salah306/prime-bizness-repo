# -*- coding: utf-8 -*-
# Part of PrimeBizness. See LICENSE file for full copyright and licensing details.

import primebizness
import primebizness.exceptions

def login(db, login, password):
    res_users = primebizness.registry(db)['res.users']
    return res_users._login(db, login, password)

def check(db, uid, passwd):
    res_users = primebizness.registry(db)['res.users']
    return res_users.check(db, uid, passwd)
